angular.module('cendApp')

.controller('LoginCtrl', function ($scope, AuthService, $ionicPopup, $state) {
	$scope.user = {
		username: '',
		password: ''
	};
	$scope.login = function() {
		AuthService.login($scope.user).then(function(msg) {
			debugger
		}, function(errMsg) {
			var alertPopup = $ionicPopup.alert({
				title: 'Login Failed',
				template: errMsg
			});
		});
	};
})
.controller('FormCtrl', function ($scope, $ionicPopup, $state) {
	$scope.personal_details = false;

	$scope.toggle = function(el) {
		if(el === 'personal'){
			$scope.personal_details = !$scope.personal_details;
		}
		if(el === 'financial'){
			$scope.financial_details = !$scope.financial_details;
		}
		if(el === 'advance_surrender'){
			$scope.advance_surrender = !$scope.advance_surrender;
		}
		if(el === 'surrender'){
			$scope.surrender = !$scope.surrender;
		}
		if(el === 'maturity'){
			$scope.maturity = !$scope.maturity;
		}
		if(el === 'banking_details'){
			$scope.banking_details = !$scope.banking_details;
		}
		if(el === 'declaration'){
			$scope.declaration = !$scope.declaration;
		}
		if(el === 'branch_clerk'){
			$scope.branch_clerk = !$scope.branch_clerk;
		}
		if(el === 'requirements'){
			$scope.requirements = !$scope.requirements;
		}
	};
})
.controller('UserCtrl', function ($scope, $ionicPopup, $state) {
	$scope.logout = function() {
		AuthService.logout();
		$state.go('notLoggedIn.home');
	};
	
})
// .controller('SignupCtrl', function ($scope, AuthService, $ionicPopup, $state) {
// 	$scope.user = {
// 		firstName:'',
// 		lastName:'',
// 		address: '',
// 		email:'',
// 		phone:'',
// 		username: '',
// 		password: '',
// 		municipality:'',
// 		ward:''
// 	};
// 	$scope.signup = function() {
// 		AuthService.signup($scope.user).then(function(msg) {
// 			var alertPopup = $ionicPopup.alert({
// 				title: 'Registration successful!',
// 				template: msg
// 			});
// 			$state.go('home-owner.dashboard');
			
// 		}, function(errMsg) {
// 			var alertPopup = $ionicPopup.alert({
// 				title: 'Login Failed',
// 				template: errMsg
// 			});
// 		});
// 	};

// })

// // .controller('HomeOwnerCtrl', function ($scope, AuthService, $ionicPopup, API_ENDPOINT, $http, $state) {
// // 	$scope.home = { ward: ''};
// // 	$scope.log = { fault: '', ward: '', username:'' };
// // 	$scope.councilor = { name: ''};

// // 	$scope.statuses = function(){
// // 		$http.get(API_ENDPOINT.url + '/councilor/status/view').then(function(result) {
// // 			$scope.statuses = result.data.statuses;
// // 		});
// // 	};

// // 	$scope.faults = function(){
// // 		$http.get(API_ENDPOINT.url + '/homeowner/fault/view').then(function(result) {
// // 			$scope.faults = result.data.faults;
// // 		});
// // 	};	
// // 	$scope.LogFault = function(){
// // 		$http.get(API_ENDPOINT.url + '/homeowner/info').then(function(result) {
// // 			$scope.log.username = result.data.username;
// // 			$scope.log.ward = result.data.ward;

// // 			$http.post(API_ENDPOINT.url + '/homeowner/fault/create', $scope.log).then(function(result){
// // 				if(result.data.success){
// // 					var alertPopup = $ionicPopup.alert({
// // 						title: "Fault Logged",
// // 						template: result.data.msg
// // 					});
// // 				} else {
// // 					var alertPopup = $ionicPopup.alert({
// // 						title: "Fault Logged failed!",
// // 						template: result.data.msg
// // 					});
// // 				}
// // 			});

// // 		});
// // 	};
	

// // 	$scope.homeowner = function(){
// // 		$http.get(API_ENDPOINT.url + '/homeowner/info').then(function(result) {
// // 			$scope.homeowner.title = result.data.title;
// // 			$scope.homeowner.firstName = result.data.fname;
// // 			$scope.homeowner.lastName = result.data.lname;
// // 			$scope.homeowner.address = result.data.address;
// // 			$scope.homeowner.email = result.data.email;
// // 			$scope.homeowner.phone = result.data.phone;
// // 			$scope.homeowner.username = result.data.username;
// // 			$scope.homeowner.municipality = result.data.municipality;
// // 			$scope.homeowner.ward = result.data.ward;
			
			
// // 		});
// // 	};
	
// // 	$scope.getCouncilor = function(){
// // 		$http.get(API_ENDPOINT.url + '/homeowner/info').then(function(result) {
// // 			$scope.home.ward = result.data.ward;
			
// // 			$http.post(API_ENDPOINT.url + '/getCouncilor', $scope.home).then(function(result){
// // 				$scope.councilor.name = result.data.firstName + " " + result.data.lastName;
// // 			});
			
// // 		});

		
// // 	};

// // 	$scope.logout = function() {
// // 		AuthService.logout();
// // 		$state.go('muniserve.home');
// // 	};
// // })


.controller('cendAppCtrl', function ($scope, $state, $ionicPopup, AuthService, AUTH_EVENTS) {
	// $scope.$on(AUTH_EVENTS.notAuthenticated, function (event) {
	// 	AuthService.logout();
	// 	$state.go('notLoggedIn.home');
	// 	var alertPopup = $ionicPopup.alert({
	// 		title: 'Session Lost',
	// 		template: 'Sorry, You have to login.'
	// 	});
	// });
})
;
