// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('cendApp', ['ionic', 'ngAria', 'ngMaterial', 'ngAnimate'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated'
})

.constant('API_ENDPOINT', {
  url: ''
})
.config(function ( $stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('notlogged', {
      url: '/menu',
      abstract: true,
      templateUrl: 'templates/menu/menu.notLoggedIn.html'
    })
    .state('notlogged.home', {
      url: '/home',
      views: {
        'side-menu': {
          templateUrl: 'templates/home.html',
          controller: 'cendAppCtrl'
        }
      }
    })
    .state('notlogged.login', {
      url: '/login',
      views: {
        'side-menu': {
          templateUrl: 'templates/login/login.html',
          controller: 'LoginCtrl'
        }
      }
    })

    .state('loggedIn', {
      url: '/user',
      abstract: true,
      templateUrl: 'templates/menu/menu.isLoggedIn.html',
      controller: 'UserCtrl'
    })
    .state('loggedIn.form1', {
      url: '/form/1',
      views: {
        'side-menu': {
          templateUrl: 'templates/form/form.page1.html',
          controller: 'FormCtrl'
        }
      }
    })
    .state('loggedIn.form2', {
      url: '/form/2',
      views: {
        'side-menu': {
          templateUrl: 'templates/form/form.page2.html',
          controller: 'FormCtrl'
        }
      }
    })
    .state('loggedIn.form3', {
      url: '/form/3',
      views: {
        'side-menu': {
          templateUrl: 'templates/form/form.page3.html',
          controller: 'FormCtrl'
        }
      }
    })
    .state('loggedIn.form4', {
      url: '/form/4',
      views: {
        'side-menu': {
          templateUrl: 'templates/form/form.page4.html',
          controller: 'FormCtrl'
        }
      }
    })
    //if none of the above statements match, use this as the fallback
    $urlRouterProvider.otherwise('/menu/home');
  })

// .run(function ($rootScope, $state, AuthService, AUTH_EVENTS) {
//   $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
//     if (!AuthService.isAuthenticated()) {
//       if ( next.name !== 'notlogged.login' && next.name !== 'notlogged.home') {
//         event.preventDefault();
//         $state.go('notlogged.home');
//       }
//     }
//   });
// })
;